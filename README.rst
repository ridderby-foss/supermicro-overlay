Supermicro Overlay
==================

This is a Funtoo overlay with only  a few packages needed to properly configure the supermicro server but with versions that is up to date with the current supermicro downloads. 

I will not actively monitor the versions unless I need to donwload. Feel free to post a ticket or make a pull-request if there is a upstream version changes needed to be included in the overlay. 

The ebuilds are copied from the Funtoo tree.

To install, clone repo somewhere (e.g: /usr/local/supiermicro)
Copy supermicro.conf to /etc/portage/repos.conf and adjust path if needed. 


=================================
How to Contribute to this Overlay
=================================

:author: Erik Ridderby
:contact: tech@rby.nu
:language: English

Greetings GitHub Users!
=======================

.. _gitlab.com/ridderby-foss/supermicro-overlay/issues: https://gitlab.com/ridderby-foss/supermicro-overlay/issues
.. _bugs.funtoo.org: https://bugs.funtoo.org

To contribute bug reports for this overlay, you can open up a GitHub issue or send
me a pull request at `gitlab.com/ridderby-foss/supermicro-overlay/issues`_.

As this overlay contain ebuild part of Funtoo Linux but with different versions then
please also open an issue at `bugs.funtoo.org`_.
